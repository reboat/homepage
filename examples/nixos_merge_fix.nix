# nix-instantiate --eval -E '(import ./examples/nixos_merge_wrong.nix).meta'

let
  nixpkgs = builtins.fetchTarball
    {
      # Descriptive name to make the store path easier to identify
      name = "nixos-unstable-2023-12-30";
      # Commit hash
      url = "https://github.com/nixos/nixpkgs/archive/b0d36bd0a420ecee3bc916c91886caca87c894e9.tar.gz";
      # Hash obtained using `nix-prefetch-url --unpack <url>`
      sha256 = "sha256:11lyp0m66fnlnyyai2clqgqqwgy868d5fymvsbj0i9my5by9an9k";
    };

  pkgs = import nixpkgs { };

  # merge an array of sets
  recursiveMerge = attrList:
    let
      f = attrPath:
        pkgs.lib.zipAttrsWith (n: values:
          if pkgs.lib.tail values == [ ]
          then pkgs.lib.head values
          else if pkgs.lib.all pkgs.lib.isList values
          then pkgs.lib.unique (pkgs.lib.concatLists values)
          else if pkgs.lib.all pkgs.lib.isAttrs values
          then f (attrPath ++ [ n ]) values
          else pkgs.lib.last values
        );
    in
    f [ ] attrList;

  config = recursiveMerge [
    {
      meta = {
        "test-first" = {
          a = "first";
        };
      };
    }
    {
      meta = {
        "test-second" = {
          b = "second";
        };
      };
    }
  ];

in
config

