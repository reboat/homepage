# nix-instantiate --eval -E '(import ./examples/nixos_merge_wrong.nix).meta'
let

  config = {
    meta = {
      "test-first" = {
        a = "first";
      };
    };
  } // {
    meta = {
      "test-second" = {
        b = "second";
      };
    };
  };

in
config
