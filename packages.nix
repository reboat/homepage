{ pkgs
, name ? ""
, plugins ? [ ]
, wikifolder ? ./.
, dataDir ? ./. + "/files"
}:
let

  src = pkgs.nix-gitignore.gitignoreSourcePure [
    ".gitlab-ci.yml"
    ".gitignore"
    "output"
    "result"
    "*.nix"
  ]
    (pkgs.lib.cleanSource wikifolder);

  srcAssets = (pkgs.lib.cleanSource dataDir);

  pluginsOpts =
    (if builtins.length plugins > 0 then "++" else "") +
    (if builtins.length plugins > 0 then builtins.concatStringsSep " ++" plugins else "");

  buildOpts = pkgs.writeText "tiddlywiki.info" (builtins.toJSON {
    "description" = "Basic client-server edition";
    "plugins" = [
      "tiddlywiki/tiddlyweb"
      "tiddlywiki/highlight"
    ];
    "themes" = [
    ];
    "languages" = [
      "de-DE"
    ];
    "build" = {
      "index" = [
        "--render"
        "$:/plugins/tiddlywiki/tiddlyweb/save/offline"
        "index.html"
        "text/plain"
      ];
      "perma" = [
        "--render"
        "$:/core/save/all"
        "index.html"
        "text/plain"
      ];
      "static" = [

        "--render"
        "$:/core/templates/static.template.html"
        "static.html"
        "text/plain"

        "--render"
        "[!is[system]]"
        "[encodeuricomponent[]addprefix[static/]addsuffix[.html]]"
        "text/plain"

        "--render"
        "$:/core/templates/static.template.css"
        "static/static.css"
        "text/plain"


      ];
    };

  });


in
pkgs.stdenv.mkDerivation {
  name = "tiddlywiki-${name}-static";
  src = src;

  phases = [ "configurePhase" "buildPhase" "installPhase" ];

  buildInputs = with pkgs; [ jq ];

  configurePhase = ''
    ${pkgs.rsync}/bin/rsync -rlt ${src}/ ./
    rm tiddlywiki.info
    cat ${buildOpts} | jq > tiddlywiki.info
  '';


  buildPhase = ''
    echo "Added themes: ${pluginsOpts}"

    mkdir output

    ${pkgs.nodePackages.tiddlywiki}/lib/node_modules/.bin/tiddlywiki \
    ${pluginsOpts} \
    ./ \
    --verbose \
    --output output \
    --build index

    find output
  '';

  installPhase = ''
    ${pkgs.rsync}/bin/rsync -av output/ $out
    #${pkgs.rsync}/bin/rsync -av $src/files $out/
    ln -fs ${srcAssets} $out/files 
  '';
}





