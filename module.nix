# You need to disable the default module
# disabledModules = [ "services/misc/tiddlywiki.nix" ];

{ config, pkgs, lib, ... }:
let
  inherit (lib) mkDefault mkEnableOption mkForce mkIf mkMerge mkOption types;
  inherit (lib)
    mapAttrs mapAttrs' mapAttrsToList nameValuePair optional optionalAttrs
    optionalString;

  wikis = config.services.tiddlywiki;


  tiddlywikiOptions = { ... }: {

    options = {

      enable = mkEnableOption "Homepage served with traefik and nginx";

      port = mkOption {
        default = 7698;
        description = "The port where tiddlywiki will be available";
        type = types.int;
      };

      username = mkOption {
        default = "tiddlywiki";
        description = "The user used for tiddlywiki";
        type = types.str;
      };

      groupname = mkOption {
        default = "tiddlywiki";
        description = "The group used for tiddlywiki";
        type = types.str;
      };

      dataDir = mkOption {
        default = "/var/lib/tiddlywiki";
        description = "The base data directory";
        type = types.str;
      };

      # filesDir = mkOption {
      #   default = "/var/lib/tiddlywiki/files";
      #   description = "directory where static assets live, will be copyed to nix-store";
      #   type = types.str;
      # };

      plugins = mkOption {
        default = [ ];
        description = "A list of paths where plugins will be available";
        type = types.listOf types.str;
      };

      credentialFile = mkOption {
        default = "";
        description = "Filename";
        type = types.str;
      };

      static = mkOption {
        default = false;
        description = "";
        type = types.bool;
      };


    };
  };


  # This create an service that change and watch
  mkService = wikiName: cfg:
    nameValuePair "tiddlywiki-${wikiName}" (mkIf (!cfg.static) {

      description = "Tiddlywiki";
      wantedBy = [ "multi-user.target" ];
      after = [ ];
      serviceConfig =
        let
          credentials = if cfg.credentialFile != "" then "credentials=${cfg.credentialFile}" else "";
          plugins =
            (if builtins.length cfg.plugins > 0 then "++" else "") +
            (if builtins.length cfg.plugins > 0 then builtins.concatStringsSep " ++" cfg.plugins else "");
        in
        {

          Type = "simple";
          #Restart = "on-failure";
          User = cfg.username;
          #StateDirectory = "tiddlywiki";
          WorkingDirectory = cfg.dataDir;
          #ExecStartPre = "${pkgs.nodePackages.tiddlywiki}/lib/node_modules/.bin/tiddlywiki ${dataDir} --init server";
          ExecStart = pkgs.writeScript "tiddlywiki-start" ''
            #!${pkgs.runtimeShell}

            if [ ! -f ${cfg.dataDir}/tiddlywiki.info ]; then
            ${pkgs.nodePackages.tiddlywiki}/lib/node_modules/.bin/tiddlywiki ${cfg.dataDir} --init server
            fi

            ${pkgs.nodePackages.tiddlywiki}/lib/node_modules/.bin/tiddlywiki \
            ${plugins} \
            ${cfg.dataDir} \
            --verbose \
            --listen \
            ${credentials} \
            port=${toString cfg.port} \
            "readers=(authenticated)"
          '';

        };

    });


  mkVirtualHosts = wikiName: cfg:
    nameValuePair "tiddlywiki-${wikiName}" (mkIf cfg.static {
      forceSSL = false;
      enableACME = false;

      listen = [
        {
          addr = "127.0.0.1";
          port = cfg.port;
        }
      ];
      root = (import ./packages.nix {
        inherit pkgs;
        name = wikiName;
        plugins = cfg.plugins;
        wikifolder = "${cfg.dataDir}";
      });
      # locations = {
      #   "^~ /files" = {
      #     autoindex = "on";
      #     root = "${cfg.filesDir}";
      #   };
      # };
    });

in
{

  ###### interface
  options = {
    services.tiddlywiki = mkOption {
      type = types.attrsOf (types.submodule tiddlywikiOptions);
      default = { };
      description = "One or more tiddlywikis";
    };
  };

  ###### implementation
  config = mkIf (wikis != { }) {
    systemd.services = mapAttrs' mkService wikis;
    services.nginx.virtualHosts = mapAttrs' mkVirtualHosts wikis;

    # systemd.paths = mapAttrs' mkPaths folders;
  };

}
